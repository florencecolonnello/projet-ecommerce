<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Product;
use App\Form\ProductType;



class ProductViewController extends Controller
{

    /**
     * @Route("/product/{id}", name="product")
     */

    public function index(int $id, ProductRepository $repo, Request $request)
    {

        $product = $repo->find($id);

        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $repo->update($form->getData());
            return $this->redirectToRoute("home");
        }


        return $this->render('product_view/index.html.twig', [

            "form" => $form->createView(),
            "product" => $product

        ]);
    }
    
    /**
     * @Route("/product/remove/{id}", name="remove_product")
     */
    public function remove(int $id, ProductRepository $repo) {
        $repo->delete($id);
        return $this->redirectToRoute("home");
    }

}
